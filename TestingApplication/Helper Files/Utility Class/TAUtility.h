//
//  TAUtility.h
//  TestingApplication
//
//  Created by Adi Boddapati on 15/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import <Foundation/Foundation.h>
#define success_block     void(^)(NSInteger result)

@interface TAUtility : NSObject

+(void)addBy:(NSInteger)firstValue secondValue:(NSInteger)secondValue compilation:(success_block)result;

@end
