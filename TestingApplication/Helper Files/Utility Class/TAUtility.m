//
//  TAUtility.m
//  TestingApplication
//
//  Created by Adi Boddapati on 15/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import "TAUtility.h"

@implementation TAUtility

+(void)addBy:(NSInteger)firstValue secondValue:(NSInteger)secondValue compilation:(success_block)result {
    NSInteger addition=firstValue+secondValue;
    result(addition);
}

@end
