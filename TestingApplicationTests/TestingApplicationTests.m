//
//  TestingApplicationTests.m
//  TestingApplicationTests
//
//  Created by Ankit Bharti on 15/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TAUtility.h"

@interface TestingApplicationTests : XCTestCase

@end

@implementation TestingApplicationTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testForAddition {
    [TAUtility addBy:4 secondValue:7 compilation:^(NSInteger result) {
        XCTAssertEqual(result, 11,@"addition should be 11");
    }];
}

@end
